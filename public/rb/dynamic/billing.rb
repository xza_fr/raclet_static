#!/usr/bin/ruby -w
# coding: utf-8

require 'nokogiri'
require 'cgi'
require 'csv'


@spc = "\n\n\n"


cgi = CGI.new

puts cgi.header

getrequest = cgi.params

serverroot='/srv/apache/raclet/live'


def inputparse(input)

  input[/[0-9a-z-]*/]  ;  end

def pronounize(input)

  input.tr('-',' ').split.map(&:capitalize).join(' ')  ;  end


typeparsed = inputparse(getrequest["type"][0])
modelparsed = inputparse(getrequest["model"][0])
tentparsed = inputparse(getrequest["tent"][0]).to_i

deliveryparsed = inputparse(getrequest["delivery"][0])

modelname   = pronounize(modelparsed).gsub(/Se/,'SE')


@newhtm = Nokogiri::HTML.fragment('', 'UTF-8')


####  opening div

pagediv = Nokogiri::XML::Node.new "div", @newhtm

pagediv[:class] = "content order"

pagediv.content = @spc


####  heading

headtag = Nokogiri::XML::Node.new "h3", @newhtm

headtag.content = "payment info"

headtagline = Nokogiri::XML::Node.new "h5", @newhtm

headtagline.content = "#{modelname} #{typeparsed[0..-2]} and/or extras"

pagediv.add_child(headtag)
pagediv.add_child(headtagline)
pagediv.add_child(@spc)

pagediv.add_child( \
  "<br>\nNote: the following address details regard " + \
  "<strong> your card's billing address</strong> only <br>\n<br>" + @spc)


####  form

@newhtm = Nokogiri::HTML.fragment('', 'UTF-8')

form = Nokogiri::XML::Node.new "form", @newhtm

form[:action]  = "/rb/dynamic/checkout.rb"
form[:method]  = "post"


form.add_child(@spc)

form.add_child("<input type=\"hidden\" name=\"type\" value=\"#{typeparsed}\"> \n")
form.add_child("<input type=\"hidden\" name=\"model\" value=\"#{modelparsed}\"> \n")
form.add_child("<input type=\"hidden\" name=\"tent\" value=\"#{tentparsed}\"> \n")
form.add_child("<input type=\"hidden\" name=\"delivery\" value=\"#{deliveryparsed}\">\n")


if getrequest["extras"] != []

  for extra in getrequest["extras"]

    extraparsed = inputparse(extra)
    form.add_child(
      "<input type=\"hidden\" name=\"extras\" value=\"#{extraparsed}\"> \n")

  end

end

form.add_child("\n\n")





####  card radio

cardtype = "Card type:<br>\n"

form.add_child(cardtype)

for brand in ['visa', 'mastercard', 'amex']

  cardbrand = Nokogiri::XML::Node.new "input", @newhtm
  cardbrand[:type] = "radio"
  cardbrand[:name] = "cardbrand"
  cardbrand[:value] = brand
  form.add_child(cardbrand)
  form.add_child("#{brand.capitalize}<br>\n")
  
end

form.add_child("<br>\n\n")


####  card expiry

form.add_child("Card expiry date:<br>\n")

selectexmonth = Nokogiri::XML::Node.new "select", @newhtm
selectexmonth[:name] = "cardexmonth"

for month in [ '1','2','3','4','5','6','7','8','9','10','11','12' ]
  
  monthselect = Nokogiri::XML::Node.new "option", @newhtm
  monthselect[:value] = month
  monthselect.content = month.rjust(2,'0')

  selectexmonth.add_child(monthselect)
  selectexmonth.add_child("\n")

end

form.add_child(selectexmonth)
form.add_child("\n\n")


years = []
year = Time.now.strftime("%Y").to_i

10.times do  ;  years << year  ;  year += 1  ;  end

  
selectexyear = Nokogiri::XML::Node.new "select", @newhtm
selectexyear[:name] = "cardexyear"

for year in years
  
  yearselect = Nokogiri::XML::Node.new "option", @newhtm
  yearselect[:value] = year
  yearselect.content = year

  selectexyear.add_child(yearselect)
  selectexyear.add_child("\n")

end

form.add_child(selectexyear)
form.add_child("<br>\n\n")


####  card & address text

def inputadd(required, type, name, pattern, minlength, maxlength)

  cardbox = Nokogiri::XML::Node.new "input", @newhtm
  
  if required != 0
    cardbox[:required] = "required"
  end

  cardbox[:placeholder] = " "
  
  cardbox[:type] = type
  cardbox[:name] = name

  if pattern != 0
    cardbox[:pattern] = pattern
  end

  if minlength != 0
    cardbox[:minlength] = minlength
  end

  if maxlength != 0
    cardbox[:maxlength] = maxlength
  end

  return cardbox
  
end



for boxname in ['name', 'cardnum', 'cardbacknum', 'bline1', 'bline2', 'bcity',\
                'bcounty', 'bpostcode', 'email', 'phone']

  case boxname
      
  when 'name'
    form.add_child("Full name, as it appears on card:<br>\n")
    form.add_child(inputadd(1, "text", boxname, '^[A-Za-z- \'.]*$', 3, 0))

  when 'cardnum'
    form.add_child("16-digit card number, front of card:<br>\n")
    form.add_child(inputadd(1, "text", boxname, '^[0-9]*$', 14, 16))
    
  when 'cardbacknum'
    form.add_child("3-digit security code, back of card:<br>\n")
    form.add_child(inputadd(1, "text", boxname, '^[0-9]*$', 3, 3))
    
  when 'bline1'
    form.add_child("First line of address:<br>\n")
    form.add_child(inputadd(1, "text", boxname, '^[0-9A-Za-z- \'.,!]*$', 5, 0))
    
  when 'bline2'
    form.add_child("Second line of address, optional:<br>\n")
    form.add_child(inputadd(0, "text", boxname, '^[0-9A-Za-z- \'.,!]*$', 0, 0))
    
  when 'bcity'
    form.add_child("City/town:<br>\n")
    form.add_child(inputadd(1, "text", boxname, '^[A-Za-z- \'.,!]*$', 2, 0))
    
  when 'bcounty'
    form.add_child("County, in full:<br>\n")
    form.add_child(inputadd(1, "text", boxname, '^[A-Za-z- \'.,!]*$', 2, 0))
    
  when 'bpostcode'
    form.add_child("Post code:<br>\n")
    form.add_child(inputadd(1, "text", boxname, '^[0-9A-Za-z- ]*$', 4, 15))
    
  when 'email'
    form.add_child("E-mail address, for invoice:<br>\n")
    form.add_child(inputadd(1, "email", boxname, 0, 6, 0))
    
  when 'phone'
    form.add_child("Phone number:<br>\n")
    form.add_child(inputadd(1, "text", boxname, '^[-+]?[0-9-]*$', 8, 0))
    
  end

  form.add_child("<br>\n\n")

end


countries = { 'AU' => 'Australia',
              'BE' => 'Belgium',
              'BR' => 'Brazil',
              'CA' => 'Canada',
              'CZ' => 'Czech Republic',
              'DK' => 'Denmark',
              'EE' => 'Estonia',
              'FI' => 'Finland',
              'FR' => 'France',
              'DE' => 'Germany',
              'GH' => 'Ghana',
              'GR' => 'Greece',
              'GL' => 'Greenland',
              'HK' => 'Hong Kong',
              'HU' => 'Hungary',
              'IS' => 'Iceland',
              'IN' => 'India',
              'IE' => 'Ireland',
              'IL' => 'Israel',
              'IT' => 'Italy',
              'JP' => 'Japan',
              'LI' => 'Liechtenstein',
              'LT' => 'Lithuania',
              'LU' => 'Luxembourg',
              'MT' => 'Malta',
              'NL' => 'Netherlands',
              'NZ' => 'New Zealand',
              'NG' => 'Nigeria',
              'NO' => 'Norway',
              'PK' => 'Pakistan',
              'PL' => 'Poland',
              'PT' => 'Portugal',
              'RO' => 'Romania',
              'RU' => 'Russia',
              'SG' => 'Singapore',
              'ZA' => 'South Africa',
              'KR' => 'South Korea',
              'ES' => 'Spain',
              'LK' => 'Sri Lanka',
              'SE' => 'Sweden',
              'CH' => 'Switzerland',
              'TW' => 'Taiwan',
              'TR' => 'Turkey',
              'GB' => 'United Kingdom',
              'US' => 'United States of America' }
 
form.add_child("Country:<br>\n") # + \

bcountry = Nokogiri::XML::Node.new "select", @newhtm
bcountry[:name] = "bcountry"
bcountry.content = "\n"


for countrycode in countries.keys

  countryselect = Nokogiri::XML::Node.new "option", @newhtm
  countryselect[:value] = "#{countrycode}"
  
  if countrycode == "GB"
    countryselect[:selected] = "selected"
  end
  
  countryselect.content = countries[countrycode]

  bcountry.add_child(countryselect)
  bcountry.add_child("\n")

end

form.add_child(bcountry)
form.add_child("<br>\n<br>" + @spc)




####  submit

alignment = Nokogiri::XML::Node.new "p", @newhtm

alignment[:class] = "buttonmove"


submit = Nokogiri::XML::Node.new "input", @newhtm

submit[:type] = "submit"
submit[:value] = "Proceed to invoice"



alignment.add_child(submit)
alignment.add_child( \
  "<br>\nYou will be able to preview your entire order before confirming payment")

form.add_child(alignment)


form.add_child(@spc)


####  push form to div, div to file, write

pagediv.add_child(form)
pagediv.add_child(@spc)
@newhtm.add_child(pagediv)

puts  File.read("#{serverroot}/htm/snippets/header-simple.htm")

puts @newhtm.to_html + @spc

puts  File.read("#{serverroot}/htm/snippets/footer-simple.htm")




