#!/usr/bin/ruby -w
# coding: utf-8

require 'nokogiri'
require 'json'
require 'cgi'
require 'csv'
require 'fileutils'


cgi = CGI.new

puts cgi.header

postrequest = cgi.params


serverroot='/srv/apache/raclet/live'
servercsvorders='/srv/apache/raclet/live/csv/orders'


@spc = "\n\n\n"

def pronounize(input)

  input.tr('-',' ').split.map(&:capitalize).join(' ')  ;  end


countrycodes = [ 'AU', 'BE', 'CA', 'CZ', 'DK', 'EE', 'FI', 'FR', 'DE', 'GH', 'GR', 'GL', 'HK', 'HU', 'IS', 'IN', 'IE', 'IL', 'IT', 'JP', 'KR', 'LI', 'LT', 'LU', 'MT', 'NL', 'NZ', 'NG', 'NO', 'PK', 'PL', 'PT', 'RO', 'RU', 'SG', 'ZA', 'ES', 'LK', 'SE', 'CH', 'TW', 'TR', 'GB', 'US' ]



####  parse input - technically could be anything


def parse(input)

  input.gsub(/\s/,'')[/[0-9a-z-]*/]

end


def parsenum(input)

  input.gsub(/[\s\-\_]/,'')[/[0-9]*/]  ;  end  


def parsealpha(input)

  input.gsub(/[\!\"\'\.\,\:\;]/,'').gsub(/\&/,'and').strip[/[\sA-Za-z-]*/]  ;  end


def parsealphanum(input)

  input.gsub(/[\!\"\'\.\:\;]/,'').gsub(/,/,' -').gsub(/\&/,'and').strip[/[\s0-9A-Za-z-]*/]

end


def parsealphaup(input)

  input.gsub(/[\s\.-]/,'')[/[A-Z][A-Z]/]  ;  end  


def parsephone(input)

  input.gsub(/\s/,'').strip[/[\s0-9+-]*/]  ;  end


def parseemail(input)

  input.strip[/[A-z0-9\.+_-]*@[A-z0-9\.+_-]*/]  ;  end



typeparsed          = parse(postrequest["type"][0])
modelparsed         = parse(postrequest["model"][0]).gsub(/\s+/,'')
tentparsed          = parse(postrequest["tent"][0]).to_i

deliveryparsed      = parse(postrequest["delivery"][0]).gsub(/\s+/,'')


cardbrandparsed     = parse(postrequest["cardbrand"][0]).strip

cardexmonthparsed   = parsenum(postrequest["cardexmonth"][0]).strip

cardexyearparsed    = parsenum(postrequest["cardexyear"][0]).strip

nameparsed          = parsealpha(postrequest["name"][0])

cardnumparsed       = parsenum(postrequest["cardnum"][0])

cardbacknumparsed   = parsenum(postrequest["cardbacknum"][0])
 
bline1parsed        = parsealphanum(postrequest["bline1"][0])

bline2parsed        = parsealphanum(postrequest["bline2"][0])

bcityparsed         = parsealpha(postrequest["bcity"][0])

bcountyparsed       = parsealpha(postrequest["bcounty"][0])

bpostcodeparsed     = parsealphanum(postrequest["bpostcode"][0]).upcase.gsub(/-/,' ')

emailparsed         = parseemail(postrequest["email"][0])

phoneparsed         = parsephone(postrequest["phone"][0])

bcountryparsed      = parsealphaup(postrequest["bcountry"][0])




extrasparsed = []

if postrequest["extras"] != []

  for extra in postrequest["extras"]

    extrasparsed << parse(extra)

  end

end




# p [ typeparsed,
#     modelparsed,
#     tentparsed,
#     extrasparsed,
#     deliveryparsed,
#     cardbrandparsed,
#     nameparsed,
#     cardnumparsed,
#     cardbacknumparsed,
#     bline1parsed,
#     bline2parsed,
#     bcityparsed,
#     bcountyparsed,
#     bpostcodeparsed,
#     bcountryparsed,
#     emailparsed,
#     phoneparsed ]




####  validate input - is it as expected ?


inputschk = {}

begin
  json = JSON.parse( \
    File.read("#{serverroot}/json/products/#{typeparsed}/#{modelparsed}.json"))
  inputschk["type"]  = 1
  inputschk["model"] = 1

rescue
  inputschk["type"]  = 0
  inputschk["model"] = 0

end





if (inputschk["type"] + inputschk["model"]) == 2

  for i in [ "tent",
             "delivery",
             "extras",
             "cardbrand",
             "cardexmonth",
             "cardexyear",
             "name",
             "cardnum",
             "cardbacknum",
             "bline1",
             "bcity",
             "bcounty",
             "bcountry",
             "bpostcode",
             "email",
             "phone"]

    inputschk[i] = 0

  end
  
  
  if tentparsed != nil \
    && (tentparsed == 0)  ||  (tentparsed == 1)
    inputschk["tent"] = 1
  end

  
  if deliveryparsed != nil \
    &&  ["courier", "blackcountry", "burcroft", "campingworld", "devonoutdoor", \
        "highbridge", "obicamping", "gorrick"].include?(deliveryparsed)
    inputschk["delivery"] = 1
  else
  end

  
  if extrasparsed == []
    inputschk["extras"] = 1

  else

    extrasprechk = 0

    extrasparsed.each do |extra|

      ####  check if extra exists and is in stock

      if json["extras"][extra] != nil  &&  json["extras"][extra][0] > 0
          extrasprechk += 1
      end

    end

    if extrasprechk == extrasparsed.length
      inputschk["extras"] = 1
    end  

  end

  
  if cardbrandparsed != nil \
     && cardbrandparsed == "mastercard"  ||  cardbrandparsed == "visa" \
     ||  cardbrandparsed == "amex"
    inputschk["cardbrand"] = 1
  end


  yearnow = Time.now.strftime("%Y").to_i
  monthnow = Time.now.strftime("%m").to_i

  if cardexmonthparsed != nil \
     &&  cardexmonthparsed.to_i < 13

    if cardexyearparsed.to_i == yearnow && cardexmonthparsed.to_i >= monthnow
      inputschk["cardexmonth"] = 1
    elsif cardexyearparsed.to_i > yearnow
      inputschk["cardexmonth"] = 1
    end
    
  end  

  
  if cardexyearparsed != nil \
    && cardexyearparsed.to_i >= yearnow  &&  cardexyearparsed.to_i < yearnow + 10
    inputschk["cardexyear"] = 1
  end  

  
  if nameparsed != nil \
    && nameparsed.length < 50  &&  nameparsed.length > 3
    inputschk["name"] = 1
  end

  
  if cardnumparsed != nil  &&  cardnumparsed.length == 16
    inputschk["cardnum"] = 1
  end

  
  if cardnumparsed != nil \
    &&  cardnumparsed.length == 14 \
    &&  cardbrandparsed == 'amex'
    inputschk["cardnum"] = 1
  end

  
  if cardbacknumparsed != nil  &&  cardbacknumparsed.length == 3
    inputschk["cardbacknum"] = 1
  end

  
  if bline1parsed != nil  &&  bline1parsed.length > 5
    inputschk["bline1"] = 1
  end

  
  if bcityparsed != nil \
    &&  bcityparsed.length < 40  &&  bcityparsed.length > 2
   inputschk["bcity"] = 1
  end
  

  if bcountyparsed != nil \
    &&  bcountyparsed.length < 40  &&  bcountyparsed.length > 2
    inputschk["bcounty"]  = 1
  end
  
  
  if bcountryparsed != nil \
     &&  countrycodes.include?(bcountryparsed)
    inputschk["bcountry"] = 1
  end  


  if bpostcodeparsed != nil \
    && bpostcodeparsed.length > 4  &&  bpostcodeparsed.length < 15
    inputschk["bpostcode"] = 1
  end

  
  if emailparsed != nil \
    && emailparsed.match(/[A-z0-9.+_-]*@[A-z.+_-]*/) != nil \
    && emailparsed.length > 5
    inputschk["email"] = 1
  end

  
  if phoneparsed.length > 7 \
    && phoneparsed.match(/[\s0-9+-]*/) != nil
    inputschk["phone"] = 1
  end


end

# p inputschk


orderreflength = 20

orderref = rand(10 ** orderreflength).to_s.rjust(orderreflength,'0')

timeepoch = Time.now.to_i


inputerrors = []

for inputchk in inputschk
  
  if inputchk[1] != 1  ;  inputerrors << inputchk[0]  ; end
  
end





####  format output


newhtm = Nokogiri::HTML.fragment('', 'UTF-8')


####  opening div

@pagediv = Nokogiri::XML::Node.new "div", newhtm

@pagediv[:class] = "content order"

@pagediv.content = @spc

                   
####  heading

headtag = Nokogiri::XML::Node.new "h3", newhtm

headtag.content = "invoice summary"

@pagediv.add_child(headtag)
@pagediv.add_child("\n<br>" + @spc)






####  throw errors or write to & read from CSV


if inputerrors != []

  
  for @error in inputerrors

    def errorout(errormsg,data)
      @pagediv.add_child("#{@error.upcase} ERROR<br>\n" + \
            "Unexpected #{errormsg} input: #{data}<br><br>" + @spc)
    end

    case @error
    when "type"
      errorout("product type", typeparsed)
    when "model"
      errorout("product model", modelparsed)
    when "tent"
      errorout("tent", tentparsed)
    when "delivery"
      errorout("delivery", deliveryparsed)
    when "extras"
      errorout("product extras", extrasparsed)
    when "cardbrand"
      errorout("card brand", cardbrandparsed)
    when "cardexmonth"
      errorout("card expiry month", cardexmonthparsed)
      errorout("card expiry year", cardexyearparsed)
    when "cardexyear"
      errorout("card expiry year", cardexyearparsed)
    when "name"
      errorout("billing name", nameparsed)
    when "cardnum"
      errorout("16-digit card number", cardnumparsed)
    when "cardbacknum"
      errorout("3-digit back card number", cardbacknumparsed)
    when "bline1"
      errorout("address line 1", bcityparsed)
    when "bcity"
      errorout("billing city", bcityparsed)
    when "bcounty"
      errorout("billing county", bcountyparsed)
    when "bcountry"
      errorout("billing country", bcountryparsed)
    when "bpostcode"
      errorout("billing postcode", bpostcodeparsed)
    when "email"
      errorout("email address", emailparsed)
    when "phone"
      errorout("phone number", phoneparsed)
    end

  end
  
  @pagediv.add_child("Please use your device's back button to adjust your input, " + \
                     "and try again." + "<br><br>" + @spc)

  
elsif inputerrors == []

  
  CSV.open("#{servercsvorders}/temp/checkouts.csv", "ab") do |csv|

    csv << [ orderref,
             timeepoch,
             typeparsed,
             modelparsed,
             tentparsed,
             extrasparsed,
             deliveryparsed,
             cardbrandparsed,
             cardnumparsed[-4..-1],
             nameparsed,
             bline1parsed,
             bline2parsed,
             bcityparsed,
             bcountyparsed,
             bpostcodeparsed,
             bcountryparsed,
             emailparsed,
             phoneparsed ]

  end


  FileUtils.cp("#{servercsvorders}/skel/card.csv", \
               "#{servercsvorders}/temp/card.csv")

  CSV.open("#{servercsvorders}/temp/card.csv", "ab") do |csv|

    csv << [ orderref,
             cardbrandparsed,
             cardnumparsed,
             cardbacknumparsed,
             cardexyearparsed,
             cardexmonthparsed ]

    
  end
  
  ####  re-open from CSV to confirm - as file grows may need adjustments

  csv = CSV.read("#{servercsvorders}/temp/checkouts.csv", :headers => true)
  order = csv.find {|row| row['orderref'] == orderref }

  csv = CSV.read("#{servercsvorders}/temp/card.csv", :headers => true)
  card = csv.find {|row| row['orderref'] == orderref }

  # csv = CSV.foreach("#{servercsvorders}/invoices.csv", \
  #                   :headers => true) do |row|
  #   if row['orderref'] == orderref ; puts row  ; end
  # end


  @pagediv.add_child("ORDER #" + order['orderref'] + "<br>\n<br>" + @spc)

  @pagediv.add_child("<strong>Billing/delivery address</strong><br>\n")

  
  for output in [ 'name',
                  'bline1',
                  'bline2',
                  'bcity',
                  'bcounty',
                  'bpostcode',
                  'bcountry',
                  'email',
                  'phone' ]

    @pagediv.add_child(order[output] + " <br>\n")

  end


  @pagediv.add_child(@spc + "<br>\n<strong>Payment method</strong><br>\n")
  
  @pagediv.add_child(card['brand'].capitalize + " <br>\n" + \
                     card['longnum'] + " " + card['backnum'] + "<br>\n" + \
                     card['exmonth'].rjust(2,'0') + "-" + card['exyear'] + \
                     "<br>\n<br>" + @spc)

  
  @pagediv.add_child("<strong>Order inventory</strong><br>\n")


  ####  totals need to initially be blank, for when saving to totals.csv
  
  invoicetotal  = 0
  modeldeposit  = 0
  extrastotal   = 0
  deliverytotal = 0
  
  
  if order['tent'] == '1'

    modeldeposit = json["price"].to_i / 10
    
    @pagediv.add_child(pronounize(order['model']) + " 10% deposit<wbr/> £" + \
                      modeldeposit.to_s + " <br>\n")

  end

  
  for extra in JSON.load(order['extras'])

    extraprice = json["extras"][extra][1]
    @pagediv.add_child(pronounize(extra) + " <wbr/> £" + extraprice.to_s + \
                      " <br>\n")
    extrastotal += extraprice

    if order['delivery'] == 'courier'

      extradelivery = json["extras"][extra][2]
      @pagediv.add_child("+ delivery <wbr/> £" + extradelivery.to_s + \
                        " <br>\n")
      deliverytotal += extradelivery

    end

    
  end


  if order['delivery'] == 'gorrick' && order['tent'] == '0'

    @pagediv.add_child("Collection of extras from "+ order['delivery'].capitalize + \
                      "<wbr/>  £0<br>\n")

  elsif order['delivery'] != 'courier'

    @pagediv.add_child("Delivery of all items to "+ order['delivery'].capitalize + \
                      "<wbr/>  £240<br>\n")
    deliverytotal += 240

  end


  invoicetotal = modeldeposit + extrastotal + deliverytotal

  @pagediv.add_child("<br>\n<strong>ORDER TOTAL: £" + \
                     invoicetotal.to_s + " </strong><br>\n<br>" + @spc)

  
  CSV.open("#{servercsvorders}/temp/totals.csv", "ab") do |csv|

    csv << [ orderref,
             invoicetotal,
             modeldeposit,
             extrastotal,
             deliverytotal
           ]

  end
  

  pleasecheck = \
    "<strong>Please check the above order summary.</strong> " + \
    "Use your device's back button if you'd " + \
    "like to make changes, or press confirm if everything is as expected.<br>\n" + \
    "Note: small changes to punctuation in name/address are intentional." + \
    "<br><br>" + @spc

  @pagediv.add_child(pleasecheck)


  ####  form

  form = Nokogiri::XML::Node.new "form", newhtm

  form[:action]  = "/rb/dynamic/confirm.rb"
  form[:method]  = "post"

  form.add_child(@spc)

  form.add_child(
    "<input type=\"hidden\" name=\"orderref\" value=\"#{order['orderref']}\">\n")


  ####  submit

  alignment = Nokogiri::XML::Node.new "p", newhtm
  
  alignment[:class] = "buttonmove"


  submit = Nokogiri::XML::Node.new "input", newhtm

  submit[:type] = "submit"
  submit[:value] = "Confirm order & pay"

  alignment.add_child(submit)
  form.add_child(alignment)
  form.add_child(@spc)

  
  ####  push form to div, div to file, write

  @pagediv.add_child(form)
  @pagediv.add_child(@spc)


end








newhtm.add_child(@pagediv)






puts  File.read("#{serverroot}/htm/snippets/header-simple.htm")

puts newhtm.to_html + @spc

puts  File.read("#{serverroot}/htm/snippets/footer-simple.htm")
