#!/usr/bin/ruby -w
# coding: utf-8

require 'nokogiri'
require 'json'
require 'cgi'


####  DO NOT CHANGE NEWLINES - CAREFULLY ADJUSTED FOR PLAIN TEXT EMAILS


serverroot='/srv/apache/raclet/live'

@spc = "\n\n\n"


cgi = CGI.new

puts cgi.header

getrequest = cgi.params


def inputparse(input)

  input[/[0-9a-z-]*/]  ;  end

def pronounize(input)

  input.tr('-',' ').split.map(&:capitalize).join(' ')  ;  end


typeparsed = inputparse(getrequest["type"][0])
modelparsed = inputparse(getrequest["model"][0])


##  the below tests if user missed input
##  first, check main selection for nil
##  else, use input, then check secondary selection for nil

tentparsed = 2


if (getrequest["tent"][0] == nil)

  selectfail = 1

else

  tentparsed = inputparse(getrequest["tent"][0]).to_i  
  
  if ( tentparsed == 0 &&
          (getrequest["extras"] == []) )

    selectfail = 2

  else  

    selectfail = 0
  
  end
  
end



modelname   = pronounize(modelparsed).gsub(/Se/,'SE')


newhtm = Nokogiri::HTML.fragment('', 'UTF-8')


####  opening div

pagediv = Nokogiri::XML::Node.new "div", newhtm

pagediv[:class] = "content order"

pagediv.content = @spc


####  heading

headtag = Nokogiri::XML::Node.new "h3", newhtm

headtag.content = "delivery selection"

headtagline = Nokogiri::XML::Node.new "h5", newhtm

headtagline.content = "#{modelname} #{typeparsed} and/or extras"

pagediv.add_child(headtag)
pagediv.add_child(headtagline)
pagediv.add_child("<br>" + @spc)


####  form

newhtm = Nokogiri::HTML.fragment('', 'UTF-8')

form = Nokogiri::XML::Node.new "form", newhtm

form[:action] = "/rb/dynamic/billing.rb"
form[:method] = "get"


form.add_child("<input type=\"hidden\" name=\"type\" value=\"#{typeparsed}\" />\n")
form.add_child("<input type=\"hidden\" name=\"model\" value=\"#{modelparsed}\" />\n")
form.add_child("<input type=\"hidden\" name=\"tent\" value=\"#{tentparsed}\" />")
form.add_child(@spc)


def inputhiddenextra(value)

  "<input type=\"hidden\" name=\"extras\" value=\"#{value}\" /> \n"  ;  end


  
if typeparsed == 'tents'

  if tentparsed == 1

    ####  paying tent deposit

    form.add_child( \
    "You have chosen to pay the deposit for a trailer tent.<br>\n " + \
    "As we cannot deliver this by post, please choose an address we can " + \
    "deliver your entire order to:<br>" + @spc )

    for option in [ ["blackcountry", "Blackcountry, W. Midlands"],
                    ["burcroft",     "Burcroft, Northampton"],
                    ["campingworld", "Camping World, W. Sussex"],
                    ["devonoutdoor", "Devon Outdoor, Devon"],
                    ["highbridge",   "Highbridge, Bristol"],
                    ["obicamping",   "Obi Camping, Cardiff"],
                    ["gorrick",      "Raclet Ltd, Reading"] ]

    form.add_child( \
    "<input type=\"radio\" name=\"delivery\" value=\"#{option[0]}\"> " + \
    "£240 #{option[1]} <br>\n")
      
    end

    form.add_child("<br>" + @spc + \
    "For a detailed list with maps, addresses, and websites of available " + \
    "collection points, see our page of " + \
    "<a href=\"/htm/sellers.htm\" target=\"_blank\">partnered sellers</a>.<br>" + \
    "\n<br>" + \
    @spc )

    
    if getrequest["extras"] != []

      for extra in getrequest["extras"] do

  
        extraparsed = inputparse(extra)

        form.add_child(
          inputhiddenextra(extraparsed) )

      end

    end
    
    
  elsif (tentparsed == 0)  &&  (getrequest["extras"] != [])

    ####  buying extras only

    json = \
      JSON.parse(File.read("#{serverroot}/json/products/tents/#{modelparsed}.json"))

    extrasdeliverytotal = 0

    form.add_child( \
    "You have chosen to purchase #{modelname} extras only.<br>\n" + \
    "Your order can be shipped by UPS delivery within 4 working days.<br>\n" + \
    "Note: to prevent fraud, we can only deliver to the billing address.<br>\n" + \
    "Alternatively, you can collect free of charge from " + \
    "Raclet Ltd, Gorrick, Wokingham, Berkshire.<br>\n" + @spc )

    for extra in getrequest["extras"] do

      extraparsed = inputparse(extra)

     
      form.add_child( \
        inputhiddenextra(extraparsed) )

      extradelivery = json["extras"][extraparsed][2].to_i

      inputhiddenextra(extraparsed)

      # form.add_child( \
      # "£#{extradelivery} delivery of #{pronounize(extraparsed)} <br>\n")

      extrasdeliverytotal += extradelivery

    end      


    form.add_child( @spc + \
    "<input type=\"radio\" name=\"delivery\" value=\"courier\"> " + \
    "£" + extrasdeliverytotal.to_s + \
    " total delivery fee for extras<br>\n" + \
    "<input type=\"radio\" name=\"delivery\" value=\"gorrick\"> " + \
    "£0 collection from Raclet Ltd.<br>" + @spc )
    
  end

end




####  submit


alignment = Nokogiri::XML::Node.new "p", newhtm

alignment[:class] = "buttonmove"


##  slip in error based on earlier check for missing input

if selectfail == 0

  submit = Nokogiri::XML::Node.new "input", newhtm

  submit[:type] = "submit"
  submit[:value] = "Proceed to card payment"

  alignment.add_child(submit)
  form.add_child(alignment)

else

  ##  if no main product selection and no extras selection
  
  form.add_child( \
  "It seems you missed a selection on the previous page.<br>\n" + \
  "Please use your device's back button and try again.<br>\n" )
  
end

form.add_child(@spc)


pagediv.add_child(form)
pagediv.add_child(@spc)


newhtm.add_child(pagediv)
newhtm.add_child(@spc)


puts  File.read("#{serverroot}/htm/snippets/header-simple.htm")

puts pagediv.to_html

puts File.read("#{serverroot}/htm/snippets/footer-simple.htm")




