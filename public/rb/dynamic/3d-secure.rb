#!/usr/bin/ruby -w

require 'cgi'
require 'cgi/session'
require 'net/https'
require 'nokogiri'
require 'fileutils'
require 'json'
require 'csv'


@cgi = CGI.new

postrequest = @cgi.params

@serverroot='/srv/apache/raclet/live'
servercsvorders = "#{@serverroot}/csv/orders"

webmasteremail = "@xza.fr"

@worldpaykey = ''

@spc = "\n\n\n"



def outputheaders

  puts @cgi.header
  puts File.read("#{@serverroot}/htm/snippets/header-simple.htm")  ;  end


def outputfooter

  puts File.read("#{@serverroot}/htm/snippets/footer-simple.htm")  ;  end


def parsenum(input)

  input[/[0-9]*/]  ;  end  


def parsebase64(input)

  input[/^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}==|[A-Za-z0-9+\/]{3}=)?$/]  ;  end


def worldpayapi(ordercode, apihash)

  begin

    uri = URI("https://api.worldpay.com/v1/orders/#{ordercode}")

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.ssl_version = :TLSv1_2

    req = Net::HTTP::Put.new(uri.path, \
          {'Content-Type'  => 'application/json', \
           'Authorization' => "#{@worldpaykey}" } )
    req.body = apihash

    res = http.request(req)
    return res.body

  rescue => error

    return error

  end

end



orderrefparsed    =  parsenum(postrequest["MD"][0])

paresparsed  =  parsebase64(postrequest["PaRes"][0])


orderfound = 0

begin
  
  csv = CSV.read("#{servercsvorders}/temp/checkouts.csv", :headers => true)
  order = csv.find { |row| row['orderref'] == orderrefparsed }

  csv = CSV.read("#{servercsvorders}/temp/totals.csv", :headers => true)
  totals = csv.find {|row| row['orderref'] == orderrefparsed }

  csv = CSV.read("#{servercsvorders}/temp/worldpay.csv", :headers => true)
  ordercodes = csv.find {|row| row['orderref'] == orderrefparsed }


  if order != nil && totals != nil && ordercodes != nil

    orderfound = 1

  end

rescue

  outputheaders

  puts "<div class=\"content order\">\n" + \
       "ERROR RECORDS<br>\n<br>\n" + \
       "Order failed - we did not attempt finish payment and debit money " + \
       "as there was an error accessing our order records.<br>\n" + \
       "Please try again, or contact #{webmasteremail} for support." + \
       "</div>"

  outputfooter
  
  exit

end



if orderfound == 1


  useragent   = ENV['HTTP_USER_AGENT']

  useraccept  = ENV['HTTP_ACCEPT']

  userip      = ENV['REMOTE_ADDR']

  session = CGI::Session.new(@cgi, 'new_session' => false)


  apihash = {
              "threeDSResponseCode" => paresparsed,
              "shopperAcceptHeader" => useraccept,
              "shopperUserAgent"    => useragent,
              "shopperSessionId"    => session.session_id,
              "shopperIpAddress"    => userip
            }


  apiresponse = worldpayapi(ordercodes['ordercode'], apihash.to_json)

  apiresponse = JSON.parse(apiresponse)

  session.close

  apipaystatus = apiresponse["paymentStatus"]


  if apipaystatus == "SUCCESS"      

    CSV.open("#{servercsvorders}/complete/invoices.csv", "ab") do |csv|

      csv << order

    end

    CSV.open("#{servercsvorders}/complete/totals.csv", "ab") do |csv|

      csv << totals

    end

    CSV.open("#{servercsvorders}/complete/worldpay.csv", "ab") do |csv|

      csv << ordercodes

    end

    Dir.glob("#{servercsvorders}/skel/*.csv") do |sourcecsv|

      csvname = sourcecsv.split('/').last

      FileUtils.cp("#{servercsvorders}/skel/#{csvname}", \
                   "#{servercsvorders}/temp/#{csvname}")

    end


    puts @cgi.header( 'status' => "302", \
           'location' => "/rb/dynamic/lookup.rb?orderref=#{orderrefparsed}&send=1" )


  else

    outputheaders

    puts \
    "<div class=\"content order\">\n" + \
    "<strong>ERROR WORLDPAY 3DS</strong><br>\n<br>\n" + \
    "Order failed - we attempted to send payment verification to Worldpay, " + \
    "but the response replied: <br>\n<br>" + @spc + \
    apipaystatus                        + "<br>\n" + \
    apiresponse["iso8583Status"].to_s   + "<br>\n" + \
    "<br>" + @spc + \
    "You may be able to use your device's back button to try again." + \
    "\n</div>"

    outputfooter

    exit


  end

    
elsif orderfound == 0

  outputheaders

  puts "<div class=\"content order\">\n" + \
       "<strong>ERROR ORDERREF #2</strong><br>\n<br>\n" + \
       "Order failed - we did not attempt to charge your card " + \
       "as the order reference number was not found in our systems.<br>\n" + \
       "Please try again, or contact #{webmasteremail} for support." + \
       "</div>"

  outputfooter

  exit
  
end

