#!/usr/bin/ruby -w
# coding: utf-8

start = Time.now.to_i

require 'nokogiri'
require 'cgi'
require 'cgi/session'
require 'json'
require 'csv'
require 'net/https'
require 'fileutils'


@cgi = CGI.new

postrequest = @cgi.params

begin
  session = CGI::Session.new(@cgi, 'new_session' => false)
  session.delete
rescue ArgumentError
end
session = CGI::Session.new(@cgi, 'new_session' => true)
session.close


@serverroot='/srv/apache/raclet/live'
servercsvorders = "#{@serverroot}/csv/orders"

@spc = "\n\n\n"

@worldpaykey = ''

webmasteremail = "@raclet.uk"


def outputheaders

  puts @cgi.header
  puts File.read("#{@serverroot}/htm/snippets/header-simple.htm")

end

def outputfooter

  puts File.read("#{@serverroot}/htm/snippets/footer-simple.htm")

end



def pronounize(input)

  input.tr('-',' ').split.map(&:capitalize).join(' ')  ;  end


def parse(input)

  input[/[0-9a-z-]*/]  ;  end


def parsenum(input)

  input[/[0-9]*/]  ;  end  


def parsealpha(input)

  input[/[\sA-Za-z-]*/]  ;  end


def parsealphanum(input)

  input[/[\s0-9A-Za-z-]*/]  ;  end


def parsealphaup(input)

  input[/[A-Z][A-Z]/]  ;  end  


def parsephone(input)

  input[/[\s0-9+-]*/]  ;  end


def parseemail(input)

  input[/[A-z0-9\.+_-]*@[A-z0-9\.+_-]*/]  ;  end



def worldpayapi(input)

  begin

    uri = URI('https://api.worldpay.com/v1/orders')

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.ssl_version = :TLSv1_2

    req = Net::HTTP::Post.new(uri.path, \
          {'Content-Type'  => 'application/json', \
           'Authorization' => "#{@worldpaykey}" } )
    req.body = input

    res = http.request(req)
    return res.body

  rescue => error

    return error    

  end

end




orderrefparsed = parsenum(postrequest["orderref"][0])



orderfound = 0

begin
  
  csv = CSV.read("#{servercsvorders}/temp/checkouts.csv", :headers => true)
  order = csv.find { |row| row['orderref'] == orderrefparsed }

  csv = CSV.read("#{servercsvorders}/temp/totals.csv", :headers => true)
  totals = csv.find {|row| row['orderref'] == orderrefparsed }

  csv = CSV.read("#{servercsvorders}/temp/card.csv", :headers => true)
  card = csv.find {|row| row['orderref'] == orderrefparsed }


  if order != nil && totals != nil && card != nil

    orderfound = 1

  end

rescue

  outputheaders

  puts "<div=\"content order\">\n" + \
       "ERROR RECORDS<br>\n" + \
       "Order failed - we did not attempt to charge your card " + \
       "as there was an error accessing our order records.<br>\n" + \
       "Please try again, or contact #{webmasteremail} for support." + \
       "</div>"

  outputfooter
  
  exit

end



if orderfound == 1


  orderpaid = 0

  csv = CSV.read("#{servercsvorders}/complete/invoices.csv", :headers => true)
  completeorder = csv.find {|row| row['orderref'] == orderrefparsed }

  if completeorder != nil

    orderpaid = 1

  end
    

  if orderpaid == 1

    puts @cgi.header( 'status'=>"302", \
                 'location'=>"/rb/dynamic/lookup.rb?orderref=#{orderrefparsed}&send=1" )

    
  elsif orderpaid == 0  &&  orderfound == 1


    ####  parse for safety
    
    cardbrand   = parse(card['brand'])

    cardnum     = parsenum(card['longnum'])

    cardbacknum = parsenum(card['backnum'])

    cardexyear  = parsenum(card['exyear'])

    cardexmonth = parsenum(card['exmonth'])

    
    name        = parsealpha(order['name'])

    bline1      = parsealphanum(order['bline1'])

    bline2      = parsealphanum(order['bline2'])

    bcity       = parsealpha(order['bcity'])

    bcounty     = parsealpha(order['bcounty'])

    bcountry    = parsealphaup(order['bcountry'])

    bpostcode   = parsealphanum(order['bpostcode'])

    email       = parseemail(order['email'])

    phone       = parsephone(order['phone'])

    ordertotal  = parsenum(totals["ordertotal"])

    userip      = ENV['REMOTE_ADDR']

    useragent   = ENV['HTTP_USER_AGENT']

    useraccept  = ENV['HTTP_ACCEPT']

    
    # MASTERCARD
      # 5555555555554444
      # 5454545454545454

    # VISA
      # 4444333322221111
      # 4917610000000000
      # 4462030000000000

    # AMEX
      # 3434343434343434

    
    ordertotalinpence = ordertotal.to_i * 100

    apihash = {
      
        "paymentMethod" => {
             "name" => "#{name}",
             "expiryMonth" => "#{cardexmonth}",
             "expiryYear" => "#{cardexyear}",
             "cardNumber" => "#{cardnum}",
             "type" => "Card",
             "cvc" => "#{cardbacknum}",
             },
        
        "amount" => ordertotalinpence,
        "is3DSOrder" => true,
        "currencyCode" => "GBP",
        "orderDescription" => "RACLET LTD",
        "customerOrderCode" => "#{orderrefparsed}",
        "settlementCurrency" =>"GBP",
        "name" => "#{name}",

        "billingAddress" => {
             "address1" => "#{bline1}",
             "postalCode" => "#{bpostcode}",
             "city" => "#{bcity}",
             "state" => "#{bcounty}",
             "countryCode" => "#{bcountry}",
             "telephoneNumber" => "#{phone}"
             },

        "deliveryAddress" => {
             "address1" => "#{bline1}",
             "postalCode" => "#{bpostcode}",
             "city" => "#{bcity}",
             "state" => "#{bcounty}",
             "countryCode" => "#{bcountry}",
             "telephoneNumber" => "#{phone}"
             },

        "shopperEmailAddress" => "#{email}",
        "shopperIpAddress" => "#{userip}",
        "shopperSessionId"  => "#{session.session_id}",
        "shopperAcceptHeader" => "#{useraccept}",
        "shopperUserAgent" => "#{useragent}"

    }


    # apiresponse = {
    #                "orderCode" => "d488477b-b222-4dbe-ae36-0fc31bffe425",
    #                "token" => "TEST_SU_ef089ba6-cf29-4f04-9e45-1c4856aca4d3",
    #                "orderDescription" => "RACLET LTD",
    #                "amount" => 500,
    #                "currencyCode" => "GBP",
    #                "paymentStatus" => "SUCCESS",
    #                "paymentResponse" => 
    #                    {"type" => "ObfuscatedCard",
    #                     "name" => "MR A APPLESAUCE",
    #                     "expiryMonth" => 1,
    #                     "expiryYear" => 2022,
    #                     "cardType" => "VISA_DEBIT",
    #                     "maskedCardNumber" => "**** **** **** 0000",
    #                     "billingAddress" => 
    #                         {"address1" => 
    #                          "19 BLAH lan",
    #                          "postalCode" => "RJ9 2AP",
    #                          "city" => "Junipter",
    #                          "state" => "Blah",
    #                          "countryCode" => "GB",
    #                          "telephoneNumber" => "+2390402898902854"},
    #                    "cardSchemeType" => "consumer",
    #                    "cardSchemeName" => "VISA DEBIT",
    #                    "cardIssuer" => "LA BANQUE POSTALE",
    #                    "countryCode" => "FR",
    #                    "cardClass" => "debit",
    #                    "cardProductTypeDescNonContactless" => "Visa Dr/Elec Per Int",
    #                    "cardProductTypeDescContactless" => "CL VisaDr/Elec PsInt",
    #                    "prepaid" => "false"},

    #                "is3DSOrder" => false,
    #                "settlementCurrency" => "GBP",
    #                "shopperEmailAddress" => "lasi@sjldan.com",
    #                "customerOrderCode" => "92857883624799344394",
    #                "environment" => "TEST",
    #                "riskScore" => 
    #                    {"value" => "1"},

    #                "resultCodes" => 
    #                    {"avsResultCode" => "APPROVED",
    #                     "cvcResultCode" => "APPROVED"}
    #               }


    apiresponse = worldpayapi(apihash.to_json)
    apiresponse = JSON.parse(apiresponse)
    
    apipaystatus = apiresponse["paymentStatus"]


    if apipaystatus == "SUCCESS"      

      CSV.open("#{servercsvorders}/complete/invoices.csv", "ab") do |csv|

        csv << order

      end

      CSV.open("#{servercsvorders}/complete/totals.csv", "ab") do |csv|

        csv << totals

      end

      CSV.open("#{servercsvorders}/complete/worldpay.csv", "ab") do |csv|

        csv << [ orderrefparsed,
                 apiresponse["orderCode"] ]

      end

      Dir.glob("#{servercsvorders}/skel/*.csv") do |sourcecsv|

        csvname = sourcecsv.split('/').last
        
        FileUtils.cp("#{servercsvorders}/skel/#{csvname}", \
                     "#{servercsvorders}/temp/#{csvname}")
        
      end


      puts @cgi.header( 'status' => "302", \
             'location' => "/rb/dynamic/lookup.rb?orderref=#{orderrefparsed}&send=1" )


      
    elsif apipaystatus == "PRE_AUTHORIZED"

      CSV.open("#{servercsvorders}/temp/worldpay.csv", "ab") do |csv|

        csv << [ orderrefparsed,
                 apiresponse["orderCode"] ]

      end

      newhtm = Nokogiri::HTML.fragment('', 'UTF-8')

      pagediv = Nokogiri::XML::Node.new "div", newhtm

      pagediv[:class] = "content order"
      
      pagediv.add_child( @spc + \
      "According to our UK-based payment processor Worldpay, 3D secure has been " + \
      "implemented for this type of card.<br>\n" + \
      "It is a means of preventing fraud by using device fingerprinting " + \
      "technologies, and sometimes a password too.<br>\n<br>\n\n" + \
      "You will now need to press the button below to verify your payment.<br>\n " + \
      "Afterwards you will be returned back to ecom.raclet.uk for your invoice." + \
      "<br>\n<br>" + @spc )


      ####  form

      form = Nokogiri::XML::Node.new "form", newhtm

      form[:action] = apiresponse["redirectURL"]
      form[:method] = "post"

      form.add_child(@spc)
      
      def inputhiddenextra(name, value)

        "<input type=\"hidden\" name=\"#{name}\" value=\"#{value}\" /> \n\n"  ;  end

      
      form.add_child(inputhiddenextra("PaReq", apiresponse["oneTime3DsToken"]))

      form.add_child(inputhiddenextra( \
                       "TermUrl", "https://ecom.raclet.uk/rb/dynamic/3d-secure.rb"))

      form.add_child(inputhiddenextra("MD", orderrefparsed))


      ####  submit


      alignment = Nokogiri::XML::Node.new "p", newhtm

      alignment[:class] = "buttonmove"


      submit = Nokogiri::XML::Node.new "input", newhtm

      submit[:type] = "submit"
      submit[:value] = "Proceed to verify payment"

      
      alignment.add_child(submit)
      form.add_child(alignment)
      form.add_child(@spc)


      pagediv.add_child(form)
      pagediv.add_child(@spc)      
      
      newhtm.add_child(pagediv)
      newhtm.add_child(@spc)


      outputheaders
      
      puts newhtm.to_html
      
      outputfooter

      exit
           
      

    elsif apipaystatus == "FAILED"

      outputheaders

      puts \
      "<div=\"content order\">\n" + \
      "<strong>ERROR WORLDPAY</strong><br>\n" + \
      "Order failed - we attempted to process payment via Worldpay, " + \
      "but the response replied: <br><br>\n" + \
      "#{apipaystatus}<br>\n" + \
      "Address verify: #{apiresponse["resultCodes"]["avsResultCode"]}<br>\n" + \
      "Card CVC verify: #{apiresponse["resultCodes"]["cvcResultCode"]}<br><br>\n" + \
      "Please use your device's back button to change your details, " + \
      "and then try again.<br>\n" + \
      "If problems persist, contact #{webmasteremail} for support." + \
      "\n</div>"

      outputfooter

      exit


    else

      outputheaders
      
      puts \
      "<div=\"content order\">\n" + \
      "<strong>ERROR WORLDPAY</strong><br>\n" + \
      "Order failed - we attempted to process payment via Worldpay, " + \
      "but the response replied: <br>\n<br>" + @spc + \
      apiresponse["httpStatusCode"].to_s  + "<br>\n" + \
      apiresponse["customCode"].to_s      + "<br>\n" + \
      apiresponse["message"].to_s         + "<br>\n" + \
      apiresponse["description"].to_s     + "<br>\n" + \
      "<br>" + @spc + \
      "You may be able to use your device's back button to amend payment details, " + \
      "and try again" + \
      "\n</div>"
      
      outputfooter

      exit

      
    end
    
    
  else

    outputheaders
    
    puts "<div=\"content order\">\n" + \
         "<strong>ERROR ORDERREF #1</strong><br>\n" + \
         "Order failed - we did not attempt to charge your card " + \
         "as the order reference number was not found in our systems.<br>\n" + \
         "Please try again, or contact #{webmasteremail} for support." + \
         "</div>"

     outputfooter

     exit


  end

  
elsif orderfound == 0

  outputheaders

  puts "<div=\"content order\">\n" + \
       "<strong>ERROR ORDERREF #2</strong><br>\n" + \
       "Order failed - we did not attempt to charge your card " + \
       "as the order reference number was not found in our systems.<br>\n" + \
       "Please try again, or contact #{webmasteremail} for support." + \
       "</div>"

  outputfooter

  exit
  
end
