#!/usr/bin/ruby -w
# coding: utf-8

require 'nokogiri'
require 'cgi'
require 'json'
require 'csv'
require 'net/smtp'
require 'net/https'


cgi = CGI.new

puts cgi.header

getrequest = cgi.params


serverroot='/srv/apache/raclet/live'

@spc = "\n\n\n"

@worldpaykey = ''


def pronounize(input)

  input.tr('-',' ').split.map(&:capitalize).join(' ')  ;  end


####  parse input - technically could be anything

def parsenum(input)

  begin
    input.gsub(/[\s\-\_]/,'')[/[0-9]*/]
  rescue
    input = 0
  end

end


def worldpayapi(ordercode)

  begin

    uri = URI("https://api.worldpay.com/v1/orders/#{ordercode}")

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.ssl_version = :TLSv1_2

    req = Net::HTTP::Get.new(uri.path, \
          {'Content-Type'  => 'application/json', \
           'Authorization' => "#{@worldpaykey}" } )

    res = http.request(req)
    return res.body

  rescue => error

    return error

  end

end



orderrefparsed  =  parsenum(getrequest["orderref"][0])

sendparsed     =  parsenum(getrequest["send"][0]).to_i


####  validate input - is it as expected ?

orderrefchk  = 0

begin
  csv = CSV.read("#{serverroot}/csv/orders/complete/invoices.csv", :headers => true)
  order = csv.find {|row| row['orderref'] == orderrefparsed }
  if order != nil  ;  orderrefchk  = 1  ; end

rescue

end


ordertotalchk  = 0

begin
  csv = CSV.read("#{serverroot}/csv/orders/complete/totals.csv", :headers => true)
  ordertotals = csv.find {|row| row['orderref'] == orderrefparsed }
  if ordertotals != nil  ;  ordertotalchk  = 1  ; end

rescue

end


ordercodechk  = 0

begin
  csv = CSV.read("#{serverroot}/csv/orders/complete/worldpay.csv", :headers => true)
  ordercodes = csv.find {|row| row['orderref'] == orderrefparsed }
  if ordertotals != nil  ;  ordercodechk  = 1  ; end

rescue

end



####  format output

newhtm = Nokogiri::HTML.fragment('', 'UTF-8')


####  opening div

@pagediv = Nokogiri::XML::Node.new "div", newhtm

@pagediv[:class] = "content order"

@pagediv.content = "\n\n"


                   
####  heading

headtag = Nokogiri::XML::Node.new "h3", newhtm

headtag.content = "INVOICE"

@pagediv.add_child(headtag)
@pagediv.add_child("\n<br>\n")


if orderrefchk == 0

  @pagediv.add_child("Unfortunately, order reference #" + orderrefparsed + \
                     " could not be found.<br>\n" + \
                     "This could either be due to a programming error, or you may " + \
                     "have provided the wrong order number." + \
                     "Note: your order number would be 20 digits long.")


elsif orderrefchk == 1

    begin
      ordertotal    = "£" + ordertotals['ordertotal'].to_s
      modeldeposit  = "£" + ordertotals['modeldeposit'].to_s
      extrastotal   = "£" + ordertotals['extrastotal'].to_s
      deliverytotal = "£" + ordertotals['deliverytotal'].to_s
    rescue
      ordertotal = ''
      modeldeposit = ''
      extrastotal = ''
      deliverytotal = ''
    end


  @pagediv.add_child("ORDER #" + order['orderref'] + "<br>\n")

  timehuman = Time.at(order['timeepoch'].to_i).strftime("%F %R")
  @pagediv.add_child(timehuman + " GMT<br>\n<br>\n")

  @pagediv.add_child("\n<strong>BILLING/DELIVERY ADDRESS</strong><br>\n\n")

  
  for output in [ 'name',
                  'bline1',
                  'bline2',
                  'bcity',
                  'bcounty',
                  'bpostcode',
                  'bcountry',
                  'email',
                  'phone' ]

    @pagediv.add_child(order[output] + " <br>\n")

  end


  @pagediv.add_child("\n\n<br><strong>PAYMENT METHOD</strong><br>\n\n")
  
  @pagediv.add_child(order['cardbrand'].capitalize + " <br>\n" \
                    "************" + order['cardnumend'] + " <br><br>" + "\n\n")

  
  @pagediv.add_child("\n<strong>ORDER INVENTORY</strong><br>\n\n")


  if order['tent'] == '1'
    
    @pagediv.add_child(pronounize(order['model']) + " 10% deposit<br>\n" + \
                      modeldeposit + " <br><br>\n\n")

  end


  if order['extras'] != "[]"

    for extra in JSON.parse(order['extras'])

    @pagediv.add_child(pronounize(extra) + " - " + pronounize(order['model']) + "<br>\n")

    end

    @pagediv.add_child(extrastotal + " <br><br>\n\n")
    
  end


  if order['delivery'] == "courier"

    @pagediv.add_child("Delivery to billing address via UPS")

  elsif order['delivery'] == "gorrick"  &&  order['tent'] == 0  

    @pagediv.add_child("Collection from Gorrick, Wokingham, RG40 3AU")

  else
                       
    @pagediv.add_child("Delivery to " + order['delivery'].capitalize)

  end
                       
  @pagediv.add_child(" \n" + deliverytotal + " <br><br>\n\n" + \
             "<strong>ORDER TOTAL: " + ordertotal + " </strong><br><br>" + @spc)



  if ordercodechk == 1

    ordercode = ordercodes['ordercode']

    apiresponse = worldpayapi(ordercode)
    apiresponse = JSON.parse(apiresponse)

    paidstatus = apiresponse["paymentStatus"]
    paidnum = ( apiresponse["amount"].to_f / 100 ).to_s

    @pagediv.add_child( \
    "<strong>WORLDPAY STATUS</strong>" + "<br>\n" + \
    "£" + paidnum + " DEBIT<br>\n" + \
    paidstatus + "<br>\n" + \
    ordercode + "<br>" + "\n\n" )

  end
  
  
  
  if sendparsed == 1

    emailbody = @pagediv.content

    sender         = '@raclet.uk'
    webmasteremail = '@xza.fr'

    smtppassword = ''

    
##    for recipient in [ '@raclet.co.uk', '@raclet.co.uk', webmasteremail, order['email'] ]
    for recipient in [ webmasteremail, order['email'] ]

      begin

        ####  no indents - ruby 2.0 does not support:  body = <<~ENDBODY

        fullemail = \
%{From: Raclet Ltd. <#{sender}>
To: #{order['name']} <#{recipient}>
Reply-to: Raclet Ltd. <mail@raclet.co.uk>
Subject: ECOM.RACLET.UK INVOICE \##{orderrefparsed}


RACLET LTD.
Trailer tents & marquees since 1969
#{emailbody}
You can also view this invoice on our website at:
https://ecom.raclet.uk/rb/dynamic/lookup.rb?orderref=#{orderrefparsed}&send=0


Raclet Ltd.
Gorrick
Luckley Road
RG40 3AU
mail@raclet.co.uk
0118 979 1023
Registered company #0946896

        }


        smtp = Net::SMTP.new('mail.gandi.net', 465)

        smtp.enable_tls

        smtp.start('mail.gandi.net', sender, smtppassword, :login ) do |s|

          s.send_message fullemail, sender, recipient

        end

      rescue
      end

    end
      
  end


end



newhtm.add_child(@pagediv)


puts File.read("#{serverroot}/htm/snippets/header-simple.htm")

puts newhtm.to_html + @spc

puts File.read("#{serverroot}/htm/snippets/footer-simple.htm")

