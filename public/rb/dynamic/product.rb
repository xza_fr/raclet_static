#!/usr/bin/ruby -w
# coding: utf-8

require 'nokogiri'
require 'json'
require 'cgi'


@spc = "\n\n\n"


serverroot="/srv/apache/raclet/live"

cgi = CGI.new

puts cgi.header

getrequest = cgi.params


def inputparse(input)

  input[/[0-9a-z-]*/]  ;  end

def pronounize(input)

  input.tr('-',' ').split.map(&:capitalize).join(' ')  ;  end


typeparsed  = inputparse(getrequest["type"][0])
modelparsed = inputparse(getrequest["model"][0])

typename    = typeparsed[0..-2]
modelname   = pronounize(modelparsed).gsub(/Se/,'SE')



newhtm = Nokogiri::HTML.fragment('', 'UTF-8')


####  opening div

pagediv = Nokogiri::XML::Node.new "div", newhtm

pagediv[:class] = "content order"

pagediv.content = @spc


####  heading

headtag = Nokogiri::XML::Node.new "h3", newhtm

headtag.content = "product selection"

headtagline = Nokogiri::XML::Node.new "h5", newhtm

headtagline.content = "#{modelname} #{typename} and/or extras"

pagediv.add_child(headtag)
pagediv.add_child(headtagline)
pagediv.add_child("\n<br>" + @spc)


####  form

newhtm = Nokogiri::HTML.fragment('', 'UTF-8')

form = Nokogiri::XML::Node.new "form", newhtm

form[:action] = "/rb/dynamic/delivery.rb"
form[:method] = "get"


form.add_child(@spc)
form.add_child("<input type=\"hidden\" name=\"type\" value=\"#{typeparsed}\" />\n")
form.add_child("<input type=\"hidden\" name=\"model\" value=\"#{modelparsed}\" />")
form.add_child(@spc)


begin
  json = \
  JSON.parse(File.read("#{serverroot}/json/products/#{typeparsed}/#{modelparsed}.json"))
  jsonexists = 1

rescue
  pagediv.add_child("ERROR: could not find product details." + @spc)
  jsonexists = 0

end


if typeparsed == 'tents'  &&  jsonexists == 1


  ####  main product

  depositpercent = 10
  
  modeldeposit = json["price"] / depositpercent

  if json["stock"] > 0

    form.add_child( \
    "Please choose whether you want this order to include the #{modelname} " + \
    "#{typename} or the #{modelname} optional extras only: <br>\n" + \
    "<input type=\"radio\" name=\"tent\" value=\"1\"> " + \
    "£#{modeldeposit} #{modelname} #{typename} #{depositpercent}% deposit<br>\n" + \
    "<input type=\"radio\" name=\"tent\" value=\"0\"> " + \
    "£0 #{modelname} extras only<br>\n<br>" + @spc)

  elsif json ["stock"] < 1

    form.add_child( \
    "Unfortunately the #{modelname} #{typename} you have selected seems to be" + \
    " out of stock.<br>" + \
    "However, you can still buy optional extras only.\n" + \
    "<input type=\"hidden\" name=\"tent\" value=\"0\"><br>\n<br>" + @spc)
    
  end


  ####  secondary product

  form.add_child( \
  "Please choose any #{modelname} extras you'd like " + \
  "to add to this order, if you want any, which are to be paid in full " + \
  "and can be posted via UPS.<br>\n")

  if (json["type"] == "tents") && (json["extras"] != nil)

    json["extras"].each do |extra|

      extraname = extra[0]
      extranameedit = pronounize(extra[0].to_s)
      extraprice = extra[1][1].to_s
      extrastock = extra[1][0]

      if extrastock > 0

        form.add_child("<input type=\"checkbox\" name=\"extras\" " + \
                      "value=\"#{extraname}\"> £#{extraprice} #{extranameedit}<br>\n")

      elsif extrastock < 1

        form.add_child( "OUT OF STOCK: #{extranameedit}<br>\n")

      end
    end
  end

  form.add_child("<br>\n\n")

  
  ####  submit

  alignment = Nokogiri::XML::Node.new "p", newhtm

  alignment[:class] = "buttonmove"
  
  submit = Nokogiri::XML::Node.new "input", newhtm

  submit[:type] = "submit"
  submit[:value] = "Proceed to delivery options"

  alignment.add_child(submit)
  form.add_child(alignment)
  form.add_child(@spc)

  
end





pagediv.add_child(form)
pagediv.add_child(@spc)
newhtm.add_child(pagediv)
newhtm.add_child(@spc)


puts  File.read("#{serverroot}/htm/snippets/header-simple.htm")

puts newhtm.to_html

puts  File.read("#{serverroot}/htm/snippets/footer-simple.htm")




