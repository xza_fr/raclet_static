#!/usr/bin/ruby -w
# coding: utf-8


require 'fileutils'


@serverroot='/srv/apache/raclet/live'
servercsvorders = "#{@serverroot}/csv/orders"


Dir.glob("#{servercsvorders}/skel/*.csv") do |sourcecsv|

  csvname = sourcecsv.split('/').last

  FileUtils.cp("#{servercsvorders}/skel/#{csvname}", \
               "#{servercsvorders}/temp/#{csvname}")

end
